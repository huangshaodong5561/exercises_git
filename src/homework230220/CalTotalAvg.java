package homework230220;
import java.util.Scanner;
/*
需求：
<主持人大赛>，有6名评委给选手打分，要求分数范围为0到100之间的整数，
选手的最后得分为：去掉最高分、最低分的4个评委的平均分
 */
public class CalTotalAvg {
    public static void main(String[] args) {
        double[] scores = enterData();
        double avg = calAvgScore(scores);
        System.out.println("平均分为："+avg);
    }

    //录入成绩
    public static double[] enterData(){
        Scanner scan = new Scanner(System.in);
        double[] scores = new double[6];
        for(int i=0;i<scores.length;i++){
            System.out.println("请输入第"+(i+1)+"个评委的分数");
            scores[i] = scan.nextDouble();
        }
        return scores;
    }

    //计算总分
    public static double calTotalScore(double[] scores){
        double total = 0.0;
        double max = scores[0];
        double min = scores[0];
        for(int i=0;i<scores.length;i++){
            if(scores[i]>max){
                max = scores[i];
            }
            if(scores[i]<min){
                min = scores[i];
            }
            total += scores[i];
        }
        return total-max-min;
    }

    //计算平均分
    public static double calAvgScore(double[] scores){
        double total = calTotalScore(scores);
        double avg = total/(scores.length-2);
        return avg;
    }
}
