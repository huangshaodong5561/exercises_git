package homework230220;
import java.util.Random;
/*
需求：生成？位验证码(大写字母、小写字母、数字，中文)
 */
public class VerificationCode {
    public static void main(String[] args) {
        String code = generateVeriCode(6);
        System.out.println(code);
    }

    public static String generateVeriCode(int len){
        String code = "";
        char[] chs = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o',
                      'p','q','r','s','t','u','v','w','x','y','z','A','B','C','D',
                      'E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S',
                      'T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7',
                      '8','9','达','内'};
        Random rand = new Random();
        for(int i=1;i<=len;i++){
            int index = rand.nextInt(chs.length);
            code += chs[index];
        }
        return code;
    }
}
