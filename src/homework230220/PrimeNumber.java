package homework230220;
/*
需求：---常见面试题
找到2-100之间的所有素数(质数)
素数：除了1和它本身外，不能被其它任何自然数整数的数
每10个换一行
 */
public class PrimeNumber {
    public static void main(String[] args) {
        int count = 0;
        for(int num=2;num<=100;num++){
            boolean flag = true;
            for(int i=2;i<=num/2;i++) {
                if(num%i==0){
                    flag = false;
                }
            }
            if(flag){
                System.out.print(num+"\t");
                count++;
                if(count%10==0){
                    System.out.println();
                }
            }
        }
    }
}
