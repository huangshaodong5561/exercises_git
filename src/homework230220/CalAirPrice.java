package homework230220;
import java.util.Scanner;
/*
需求：
机票价格按照淡季旺季、头等舱、商务舱和经济舱收费，
输入机票原价、月份和头等舱、商务舱或经济舱，
其中旺季5-10月，头等舱9折，商务舱85折，经济舱8折，
淡季11月到来年4月，头等舱7折，商务舱65折，经济舱6折，
最终输出机票价格
 */
public class CalAirPrice {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入机票原价");
        double price = scan.nextDouble();
        System.out.println("请输入月份");
        int month = scan.nextInt();
        System.out.println("请输入舱位：1.头等舱 2.商务舱 3.经济舱");
        int type = scan.nextInt();
        double finalPrice = calFinalPrice(price,month,type);
        if(finalPrice!=1){
            System.out.println("机票最终价格为："+finalPrice);
        }
    }

    public static double calFinalPrice(double price,int month,int type){
        double finalPrice = 0.0;
        if(month<1 || month>12){
            System.out.println("请重新输入月份");
            return -1;
        }
        if(type<1 || type>3){
            System.out.println("请重新输入舱位");
            return -1;
        }
        if(month>=5 && month<=10){
            switch(type){
                case 1:
                    finalPrice = price*0.9;
                    break;
                case 2:
                    finalPrice = price*0.85;
                    break;
                case 3:
                    finalPrice = price*0.8;
                    break;
            }
        }else{
            switch(type){
                case 1:
                    finalPrice = price*0.7;
                    break;
                case 2:
                    finalPrice = price*0.65;
                    break;
                case 3:
                    finalPrice = price*0.6;
                    break;
            }
        }
        return finalPrice;
    }
}




