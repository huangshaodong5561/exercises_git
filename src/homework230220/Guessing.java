package homework230220;
/*
猜数字小游戏while(true)版本
训练目标：while(true)自造死循环,通常与break搭配使用
 */
import java.util.Scanner;
public class Guessing {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num = (int)(Math.random()*1000+1);
        System.out.println(num);
        while(true){
            System.out.println("猜吧");
            int guess = scan.nextInt();
            if(guess>num){
                System.out.println("猜大了");
            }else if(guess<num){
                System.out.println("猜小了");
            }else{
                System.out.println("恭喜你猜对了！");
                break;
            }
        }
    }
}
