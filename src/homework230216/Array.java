package homework230216;
/*
数组小代码练习：声明、初始化、访问、遍历
 */
public class Array {
    public static void main(String[] args) {
        //数组的声明
        int[] arr = new int[3];

        //数组的初始化
        int[] arr1 = new int[3];
        int[] arr2 = {2,5,8};
        int[] arr3 = new int[]{2,5,8};
        int[] arr4;
        arr4 = new int[]{2,5,8};

        //数组的访问
        int[] arr5 = new int[3];
        System.out.println(arr.length);
        arr[0] = 100;
        arr[1] = 200;
        arr[2] = 300;
        System.out.println(arr[arr.length-1]);

        //数组的遍历
        int[] arr6 = new int[10];
        for (int i=0;i<arr6.length;i++) {
            arr6[i] = (int)(Math.random()*100);
            System.out.println(arr6[i]);
        }
    }
}
