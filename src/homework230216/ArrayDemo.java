package homework230216;
import java.util.Arrays;
/*
对数组进行升序排列，并输出排序后的结果
 */
public class ArrayDemo {
    public static void main(String[] args) {
        int[] arr = new int[10];
        for(int i=0;i<arr.length;i++){
            arr[i] = (int)(Math.random()*100);
            System.out.println(arr[i]);
        }
        System.out.println("排序后");
        Arrays.sort(arr);
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
    }
}
