package homework230216;
/*
MultiTable九九乘法表
要求：输出九九乘法表
 */
public class MultiTable {
    public static void main(String[] args) {
        for(int i=1;i<=9;i++){
            for(int num=1;num<=i;num++){
                System.out.print(i+"*"+num+"="+i*num+"\t");
            }
            System.out.println();
        }
    }
}
