package homework230303;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortString {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("jack");
        list.add("rose");
        list.add("tom");
        list.add("jerry");
        list.add("black");
        list.add("Kobe");
        System.out.println("list:"+list); //list:[jack, rose, tom, jerry, black, Kobe]

        Collections.sort(list);
        System.out.println(list); //[Kobe, black, jack, jerry, rose, tom]

        List<String> list1 = new ArrayList<>();
        list1.add("境界");
        list1.add("鬼灭之刃无限列车");
        list1.add("斗破苍穹");
        list1.add("海贼王");
        list1.add("刺客五六七");
        list1.add("Kobe");
        System.out.println(list1); //[境界, 鬼灭之刃无限列车, 斗破苍穹, 海贼王, 刺客五六七, Kobe]

        Collections.sort(list1, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return o1.length()-o2.length();
            }
        });
        System.out.println(list1); //[境界, 海贼王, 斗破苍穹, Kobe, 刺客五六七, 鬼灭之刃无限列车]
    }
}
