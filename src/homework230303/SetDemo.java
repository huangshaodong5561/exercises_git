package homework230303;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("two");
        list.add("five");
        list.add("one"); //[one, two, three, four, two, five, one]
        System.out.println(list);

        Set<String> set = new HashSet<>();
        set.addAll(list);
        System.out.println(set); //[four, one, two, three, five]
    }
}
