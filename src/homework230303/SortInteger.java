package homework230303;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SortInteger {
    public static void main(String[] args) {
        Random random = new Random();
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<10;i++){
            list.add(random.nextInt(100));
        }
        System.out.println("list:"+list); //list:[0, 23, 46, 78, 7, 36, 89, 86, 33, 76]

        Collections.sort(list);
        System.out.println("list:"+list); //list:[15, 18, 22, 26, 40, 57, 64, 75, 82, 94]

        Collections.reverse(list);
        System.out.println("list:"+list); //list:[90, 78, 66, 63, 57, 41, 33, 33, 17, 2
    }
}
