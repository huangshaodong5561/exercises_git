package homework230303;

import java.util.ArrayList;
import java.util.List;

public class SortPoint {
    public static void main(String[] args) {
        List<Point> list = new ArrayList<>();
        list.add(new Point(5,20));
        list.add(new Point(10,23));
        list.add(new Point(15,30));
        list.add(new Point(26,40));
        list.add(new Point(5,10));
        list.add(new Point(8,32));
        System.out.println("list:"+list);

        list.sort((o1,o2)->{
            int len1 = o1.getX()*o1.getX()+o1.getY()*o1.getY();
            int len2 = o2.getX()*o2.getX()+o2.getY()*o2.getY();
            return len1-len2;
        });
        System.out.println("list:"+list);
    }
}
