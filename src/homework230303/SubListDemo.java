package homework230303;

import java.util.ArrayList;
import java.util.List;

public class SubListDemo {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<10;i++){
            list.add(i*10);
        }
        System.out.println("list:"+list); //list:[0, 10, 20, 30, 40, 50, 60, 70, 80, 90]

        List<Integer> subList = list.subList(3,7);
        System.out.println("subList:"+subList); //subList:[30, 40, 50, 60]

        for(int i=0;i<subList.size();i++){
            subList.set(i,subList.get(i)*10);
        }
        System.out.println("sublist:"+subList); //sublist:[300, 400, 500, 600]
        System.out.println("list:"+list); //list:[0, 10, 20, 300, 400, 500, 600, 70, 80, 90]

        list.set(3,200);
        System.out.println("list:"+list); //list:[0, 10, 20, 200, 400, 500, 600, 70, 80, 90]
        System.out.println("subList:"+subList); //subList:[200, 400, 500, 600]
    }
}
