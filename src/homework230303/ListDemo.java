package homework230303;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        list.add("two");
        System.out.println("list："+list); //list：[one, two, three, four, five, two]

        String e = list.get(2);
        System.out.println(e); //three

        String old = list.set(3,"one");
        System.out.println(old); //four
        System.out.println("list："+list); //list：[one, two, three, one, five, two]

        String s = list.remove(2);
        System.out.println(s); //three
        System.out.println("list："+list); //list：[one, two, one, five, two]

        list.add(3,"six");
        System.out.println("list："+list); //list：[one, two, one, six, five, two]
    }
}
