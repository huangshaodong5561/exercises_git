package homework230316;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Sever5 {
    private ServerSocket serverSocket;
    private List<PrintWriter> allOut = new ArrayList<>();

    public Sever5(){
        try {
            System.out.println("服务端正在启动");
            serverSocket = new ServerSocket(8080);
            System.out.println("服务端启动完毕");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(){
        try {
            while(true){
                System.out.println("等待客户端连接");
                Socket socket = serverSocket.accept();
                System.out.println("一个客户端连接成功");
                ClientHandler handler = new ClientHandler(socket);
                Thread t = new Thread(handler);
                t.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Sever5 sever5 = new Sever5();
        sever5.start();
    }

    private class ClientHandler implements Runnable{
        private Socket socket;
        private String host;

        public ClientHandler(Socket socket) {
            this.socket = socket;
            host = socket.getInetAddress().getHostAddress();
        }

        public void run(){
            PrintWriter pw =null;
            try {
                InputStream in = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(in, StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr);

                OutputStream out = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(out,StandardCharsets.UTF_8);
                BufferedWriter bw = new BufferedWriter(osw);
                pw = new PrintWriter(bw,true);

                synchronized (allOut){
                    allOut.add(pw);
                }

                sendMessage(host+"上线了，当前在线人数"+allOut.size());

                String message;
                while ((message= br.readLine())!=null){
                    sendMessage(host+"说"+message);
                }
            } catch (IOException e) {
            }finally {
                synchronized (allOut){
                    allOut.remove(pw);
                }
                sendMessage(host+"下线了，当前在线人数:"+allOut.size());
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void sendMessage(String message){
            System.out.println(message);
            synchronized (allOut){
                for(PrintWriter o: allOut){
                    o.println(message);
                }
            }
        }
    }
}
