package homework230306;

import java.io.File;
import java.util.Scanner;

/*
要求用户在控制台输入一个目录名，然后将这个目录创建在当前项目目录下
 */
public class Test03 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入目录");
        File dir = new File(scan.nextLine());
        if(dir.exists()){
            System.out.println("该目录已存在");
        }else{
            dir.mkdirs();
            System.out.println("创建完毕！");
        }
    }
}
