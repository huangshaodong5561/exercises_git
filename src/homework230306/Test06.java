package homework230306;

import java.io.File;

/*
列出当前目录中所有名字包含s的子项。要求：使用匿名内部类和lambda两种写法
 */
public class Test06 {
    public static void main(String[] args) {
        //匿名内部类
        /*
        File dir = new File(".");
        if(dir.isDirectory()){
            File[] subs = dir.listFiles(new FileFilter() {
                public boolean accept(File file) {
                    return file.getName().contains("s"); //因为若字符串不存在，则返回-1，可以写成return file.getName().indexOf("s")!=-1;
                }
            });
            for(File sub : subs){
                System.out.println(sub.getName());
            }
        }
         */

        //lambda版
        File dir = new File(".");
        if(dir.isDirectory()){
            File[] subs = dir.listFiles(file->file.getName().contains("s"));
            for(File sub : subs){
                System.out.println(sub.getName());
            }
        }
    }
}
