package homework230301;

public class MatchesDemo {
    public static void main(String[] args) {
        /*
        邮箱正则表达式
        [a-zA-Z0-9_]+@[a-zA-Z0-9]+(\.[a-zA-Z]+)+
         */
        String email = "247547521@qq.com";
        String regex = "[a-zA-Z0-9_]+@[a-zA-Z0-9]+(\\.[a-zA-Z]+)+";
        boolean match = email.matches(regex);
        if(match){
            System.out.println("是正确的邮箱格式");
        }else{
            System.out.println("不是正确的邮箱格式");
        }
    }
}
