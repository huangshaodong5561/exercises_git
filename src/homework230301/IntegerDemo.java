package homework230301;

public class IntegerDemo {
    public static void main(String[] args) {
        //定义
        Integer i1 = new Integer(5);
        Integer i2 = new Integer(5);
        System.out.println(i1==i2); //false
        System.out.println(i1.equals(i2)); //true

        Integer i3 = Integer.valueOf(5);
        Integer i4 = Integer.valueOf(5);
        System.out.println(i3==i4); //true
        System.out.println(i3.equals(i4)); //true

        //包装类转换为基本类型
        int i = i4.intValue();
        System.out.println(i);

        //装箱
        Integer i5 = 5;
        //拆箱
        int i6 = i5;

        //包装类的应用：
        //1)输出基本类型的取值范围
        int max = Integer.MAX_VALUE;
        int min = Integer.MIN_VALUE;
        System.out.println("int的最大值为："+max+","+"int的最小值为："+min);

        //2)将String类型转为基本类型
        String s = "28";
        int age = Integer.parseInt(s);
        System.out.println(age);

        String  s1 = "12.56";
        double price = Double.parseDouble(s1);
        System.out.println(price);
    }
}
