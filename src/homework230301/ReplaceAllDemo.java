package homework230301;

public class ReplaceAllDemo {
    public static void main(String[] args) {
        String line = "123.456.789";
        String s1 = line.replaceAll("\\.","#number#");
        System.out.println(s1); //123#number#456#number#789
    }
}
