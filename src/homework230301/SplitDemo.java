package homework230301;

import java.util.Arrays;

public class SplitDemo {
    public static void main(String[] args) {
        String line = "123.456.789";
        String[] data = line.split("\\.");
        System.out.println(Arrays.toString(data)); //[123, 456, 789]

        line = "abc123def456ghi";
        data = line.split("[0-9]+");
        System.out.println(Arrays.toString(data)); //[abc, def, ghi]

        line = ".123.456..789....156..........";
        data = line.split("\\.");
        System.out.println(Arrays.toString(data)); //[, 123, 456, , 789, , , , 156]
        System.out.println(data.length); //9
    }
}
