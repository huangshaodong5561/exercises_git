package exercises230306;

import java.util.Scanner;

/*
题目：企业发放的奖金根据利润提成。利润(I)低于或等于10万元时，奖金可提10%；
利润高于10万元，低于20万元时，低于10万元的部分按10%提成，高于10万元的部分，可可提成7.5%；
20万到40万之间时，高于20万元的部分，可提成5%；
40万到60万之间时高于40万元的部分，可提成3%；
60万到100万之间时，高于60万元的部分，可提成1.5%，
高于100万元时，超过100万元的部分按1%提成， 从键盘输入当月利润I，求应发放奖金总数？
 */
public class Exercises6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入当月利润");
        double I = scan.nextDouble();
        double bonus= 0.0;
        if(I<=100000){
            bonus = I*0.1;
        }else if(I<200000){
            bonus = 100000*0.1+(I-100000)*0.075;
        }else if(I<400000){
            bonus = 200000*0.1+(I-200000)*0.05;
        }else if(I<600000){
            bonus = 400000*0.1+(I-400000)*0.03;
        }else if(I<1000000){
            bonus = 600000*0.1+(I-600000)*0.015;
        }else{
            bonus = 1000000*0.1+(I-1000000)*0.01;
        }
        System.out.println("应发放奖金总额为"+bonus);
    }
}
