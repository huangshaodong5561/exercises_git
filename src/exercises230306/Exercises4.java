package exercises230306;
/*
题目：利用条件运算符的嵌套来完成此题：学习成绩>=90分的同学用A表示，60-89分之间的用B表示，60分以下的用C表示。
 */
public class Exercises4 {
    public static void main(String[] args) {
        int score=59;
        if(score<0 || score>100){
            System.out.println("成绩不合法");
        }else if(score>=90){
            System.out.println("A");
        }else if(score>=60){
            System.out.println("B");
        }else{
            System.out.println("C");
        }
    }
}
