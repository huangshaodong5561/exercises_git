package exercises230306;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
/*
题目：输入三个整数x,y,z，请把这三个数由小到大输出。
 */
public class Exercises7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入整数");
        int x = scan.nextInt();
        int y = scan.nextInt();
        int z = scan.nextInt();
        List<Integer> list = new ArrayList<>();
        list.add(x);
        list.add(y);
        list.add(z);
        Collections.sort(list);
        System.out.println(list);
    }
}
