package exercises230306;
/*
题目：判断101-200之间有多少个素数，并输出所有素数。
 */
public class Exercises2 {
    public static void main(String[] args) {
        for(int i=101;i<=200;i++){
            boolean flag = true;
            for(int num=2;num<=i/2;num++){
                if(i%num==0){
                    flag = false;
                    break;
                }
            }
            if(flag){
                System.out.print(i+"\t");
            }
        }
    }
}
