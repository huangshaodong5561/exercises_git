package exercises230306;

import java.util.Scanner;

/*
1、題目：古典问题：有一对兔子，从出生后第3个月起每个月都生一对兔子，小兔子长到第三个月后每个月又生一对兔子，假如兔子都不死，问12个月的兔子总数为多少？
 */
public class Exercises1 {
    public static void main(String[] args) {
        while (true) {
            System.out.println("输入整数n,求第n个斐波那契数");
            int n = new Scanner(System.in).nextInt();
            long result = f(n);
            System.out.printf("第%d个斐波那契数是: %d\n", n, result);
        }
    }

    private static long f(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return 1;
        }
        return f(n - 1) + f(n - 2); //递归
    }

    //用for循环写
    private static long ff(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return 1;
        }
        int a = 1;
        int b = 1;
        for (int i = 3; i <= n; i++) {
            b = a + b;
            a = b - a;
        }
        return b;
    }
}
