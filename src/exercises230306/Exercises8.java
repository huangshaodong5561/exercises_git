package exercises230306;
/*
输出打印9*9口诀表。
 */
public class Exercises8 {
    public static void main(String[] args) {
        for(int i=1;i<=9;i++){
            for(int num=1;num<=i;num++){
                System.out.print(i+"*"+num+"="+i*num+"\t");
            }
            System.out.println();
        }
    }
}
