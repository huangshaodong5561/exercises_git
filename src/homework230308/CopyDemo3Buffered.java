package homework230308;

import java.io.*;

public class CopyDemo3Buffered {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("JAVA.jpg");
        BufferedInputStream bis = new BufferedInputStream(fis);
        FileOutputStream fos = new FileOutputStream("JAVA3.jpg");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        long start = System.currentTimeMillis();
        int d;
        while((d=bis.read())!=-1){
            bos.write(d);
        }
        long end = System.currentTimeMillis();
        System.out.println("复制完毕！耗时"+(end-start)+"ms");
        bis.close();
        bos.close();
    }
}
