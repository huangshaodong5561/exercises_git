package homework230308;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;


public class OOSDemo {
    public static void main(String[] args) throws IOException {
        String name = "张三";
        int age = 25;
        String gender = "男";
        String[] otherInfo = {"是一名学生","喜欢打篮球","广东东莞的","喜欢王老师"};
        Student zs = new Student(name,age,gender,otherInfo);
        System.out.println(zs);
        FileOutputStream fos = new FileOutputStream("test.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(zs);
        System.out.println("写入完毕");
        oos.close();
    }
}
