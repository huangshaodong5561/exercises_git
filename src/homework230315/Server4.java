package homework230315;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Server4 {
    private ServerSocket serverSocket;

    private List<PrintWriter> allOut = new ArrayList<>();

    public Server4(){
        try {
            System.out.println("服务端正在启动");
            serverSocket = new ServerSocket(8080);
            System.out.println("服务端启动完毕");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(){
        try {
            while (true){
                System.out.println("客户端等待连接");
                Socket socket = serverSocket.accept();
                System.out.println("一个客户端连接成功");
                ClientHandler handler = new ClientHandler(socket);
                Thread t = new Thread(handler);
                t.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Server4 server4 = new Server4();
        server4.start();
    }

    private class ClientHandler implements Runnable{
        private Socket socket;
        private String host;

        public ClientHandler(Socket socket) {
            this.socket = socket;
            host = socket.getInetAddress().getHostAddress();
        }

        public void run(){
            try {
                InputStream in = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(in, StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr);

                OutputStream out = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(out,StandardCharsets.UTF_8);
                BufferedWriter bw = new BufferedWriter(osw);
                PrintWriter pw = new PrintWriter(bw,true);

                allOut.add(pw);

                String message;
                while((message=br.readLine())!=null){
                    System.out.println(host+"说："+message);

                    for(PrintWriter o :allOut){
                        o.println(host+"说："+message);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}


