package homework230227;

public class EnumDemo {
    public static void main(String[] args) {
        Season[] seasons = Season.values();
        for(int i=0;i<seasons.length;i++){
            System.out.println(seasons[i]);
            System.out.println(seasons[i].getSeasonName());
            System.out.println(seasons[i].getSeasonDesc());
       }

        Season s = Season.SPRING;
        System.out.println(s.getSeasonName()+","+s.getSeasonDesc());
        switch(s){
            case SPRING:
                System.out.println(s.getSeasonName()+"天气"+s.getSeasonDesc()+"该去放风筝咯！");
                break;
            case SUMMER:
                System.out.println(s.getSeasonName()+"天气"+s.getSeasonDesc()+"该去游泳咯！");
                break;
            case AUTUMN:
                System.out.println(s.getSeasonName()+"天气"+s.getSeasonDesc()+"该去摘苹果咯！");
                break;
            case WINTER:
                System.out.println(s.getSeasonName()+"天气"+s.getSeasonDesc()+"该去打雪仗咯！");
                break;
        }
    }
}
