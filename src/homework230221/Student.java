package homework230221;

public class Student {
    String name;
    int age;
    String className;
    String stuId;

    Student(){
    }

    Student(String name,int age){
        this.name = name;
        this.age = age;
    }

    Student(String name,int age,String className,String stuId){
        this.name = name;
        this.age = age;
        this.className = className;
        this.stuId = stuId;
    }

    void study(){
        System.out.println(name+"在学习");
    }

    void sayHi(){
        System.out.println("大家好，我叫"+name+",今年"+age+"岁了，班级名称为："+className+",学号为"+stuId);
    }

    void playWith(String anotherName){
        System.out.println(name+"在跟"+anotherName+"一起玩...");
    }
}
