package homework230221;

public class ConsDemo {
    public static void main(String[] args) {
        Student zs = new Student();
        zs.name = "张三";
        zs.age = 20;
        zs.className = "jsd2302";
        zs.stuId = "001";
        zs.study();
        zs.sayHi();
        zs.playWith("李四");

        Student ls = new Student("李四",21);
        ls.study();
        ls.sayHi();
        ls.playWith("王五");

        Student ww = new Student("王五",22,"jsd2302","003");
        ww.study();
        ww.sayHi();
        ww.playWith("赵六");

    }
}
