package homework230221;

public class CarTest {
    public static void main(String[] args) {
        Car dz = new Car();
        dz.brand = "大众";
        dz.color = "白";
        dz.price = 150000;
        dz.star();
        dz.run();
        dz.stop();

        Car bsj = new Car("保时捷","红",1200000);
        bsj.star();
        bsj.run();
        bsj.stop();
    }
}
