package homework230221;

public class Car {
    String brand;
    String color;
    double price;

    Car(){
    }

    Car(String brand,String color,double price){
        this.brand = brand;
        this.color = color;
        this.price = price;
    }

    void star(){
        System.out.println(brand+"牌子的"+color+"颜色的"+price+"块钱的汽车启动了...");
    }

    void run(){
        System.out.println(brand+"牌子的"+color+"颜色的"+price+"块钱的汽车开跑了...");
    }

    void stop(){
        System.out.println(brand+"牌子的"+color+"颜色的"+price+"块钱的汽车停下来了...");
    }
}
