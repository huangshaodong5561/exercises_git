package homework230215;
/*
for循环：输出5次"行动是成功的阶梯"、输出9的乘法表(1到9、1/3/5/7/9、9到1)
 */
public class HomeWork4 {
    public static void main(String[] args) {
        //输出5次"行动是成功的阶梯"
        for(int i=0;i<5;i++){
            System.out.println("行动是成功的阶梯");
        }
        System.out.println("继续执行...");

        //输出9的乘法表
        for(int i=1;i<=9;i++){
            System.out.println(i+"*9="+i*9);
        }

        //输出9的乘法表(1/3/5/7/9)
        for(int i=1;i<=9;i+=2){
            System.out.println(i+"*9="+i*9);
        }

        //输出9的乘法表(9到1)
        for(int i=9;i>=1;i--){
            System.out.println(i+"*9="+i*9);
        }
    }
}
