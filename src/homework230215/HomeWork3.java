package homework230215;
import java.util.Scanner;
/*
Guessing猜数字之do...while版
要求：随机生成一个数，由用户来猜，猜不对则反复猜，并给出大小提示，猜对的则结束，用do...while来实现。
 */
public class HomeWork3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int num =(int)(Math.random()*1000+1);
        System.out.println(num);
        int guess;
        do{
            System.out.println("猜吧！");
            guess = scan.nextInt();
            if(guess>num){
                System.out.println("猜大了");
            }else if(guess<num){
                System.out.println("猜小了");
            }else{
                System.out.println("恭喜你猜对了");
            }
        }while(guess!=num);
    }
}
