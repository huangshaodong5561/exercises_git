package homework230215;
import java.util.Scanner;
/*
1.CommandBySwitch命令解析程序：要求：接收用户输入的命令，依据命令做不同的操作
 */
public class HomeWork1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入命令: 1.存款业务 2.取款业务 3.转账业务 4.退卡");
        int command = scan.nextInt();
        switch(command){
            case 1:
                System.out.println("存款业务");
                break;
            case 2:
                System.out.println("取款业务");
                break;
            case 3:
                System.out.println("转账业务");
                break;
            case 4:
                System.out.println("退卡");
                break;
            default:
                System.out.println("请重新输入");
        }
    }
}
