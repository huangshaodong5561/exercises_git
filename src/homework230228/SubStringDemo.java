package homework230228;

public class SubStringDemo {
    public static void main(String[] args) {
        String str = "www.tedu.com.cn";
        int start = str.indexOf(".")+1; //写活起始下标
        int end = str.indexOf(".",start); //写活结束下标
        String name = str.substring(start,end);
        System.out.println(name);
    }
}
