package homework230228;

public class StringBuilderDemo {
    public static void main(String[] args) {
        String str = "学习Java";
        StringBuilder builder1 = new StringBuilder(str);

        builder1.append(",就是为了找个好工作");
        System.out.println(builder1); //学习Java,就是为了找个好工作

        builder1.replace(11,16,"改变世界");
        System.out.println(builder1); //学习Java,就是为了改变世界

        builder1.delete(0,6);
        System.out.println(builder1); //,就是为了改变世界

        builder1.insert(0,"活着");
        System.out.println(builder1);
    }
}
