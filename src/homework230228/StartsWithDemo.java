package homework230228;

public class StartsWithDemo {
    public static void main(String[] args) {
        String str = "thinking in java";
        boolean starts = str.startsWith("think");
        System.out.println(starts); //true

        boolean ends = str.endsWith(".png");
        System.out.println(ends); //false
    }
}
