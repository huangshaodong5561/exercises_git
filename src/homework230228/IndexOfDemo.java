package homework230228;

public class IndexOfDemo {
    public static void main(String[] args) {
        String str = "thinking in java";
        int index = str.indexOf("in");
        System.out.println(index); //2
        index = str.indexOf("in",3);
        System.out.println(index); //5
        index = str.indexOf("abc");
        System.out.println(index); //-1

        index = str.lastIndexOf("in");
        System.out.println(index); //9
    }
}
