package homework230217;
import java.util.Arrays;
public class ArrayDemo {
    public static void main(String[] args) {
        //数组的复制
        int[] a = {10,20,30,40,50};
        int[] b = new int[6];
        System.arraycopy(a,1,b,2,4);
        for(int i=0;i<b.length;i++){
            System.out.println(b[i]);
        }

        int[] c = {5,6,7,8,9,10};
        int[] d = new int[7];
        d = Arrays.copyOf(c,7);
        for(int i=0;i<d.length;i++){
            System.out.println(d[i]);
        }

        //数组的扩容
        a = Arrays.copyOf(a,a.length+1);
        for(int i=0;i<a.length;i++){
            System.out.println(a[i]);
        }
    }
}
