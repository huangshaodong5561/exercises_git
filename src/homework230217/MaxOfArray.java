package homework230217;
/*
MaxOfArray求数组元素最大值并放在最后一个元素的下一个位置
要求：声明整型数组并填充随机数据，找到最大值，将数组扩容并将最大值放末尾
 */
import java.util.Arrays;
public class MaxOfArray {
    public static void main(String[] args) {
        int[] arr = new int[10];
        for(int i=0;i<arr.length;i++){
            arr[i] = (int)(Math.random()*100);
            System.out.println(arr[i]);
        }
        int max = arr[0];
        for(int i=1;i<arr.length;i++){
            if(arr[i]>max){
                max = arr[i];
            }
        }
        System.out.println("最大值为"+max);
        arr = Arrays.copyOf(arr,arr.length+1);
        arr[arr.length-1] = max;
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
    }
}
