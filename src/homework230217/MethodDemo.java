package homework230217;
/*
方法的练习：;
要求：
- 定义say()无参无返回值方法，调用测试
- 定义sayHi()有一个参无返回值方法，调用测试
- 定义sayHello()有两个参无返回值的方法，调用测试
- 定义getNum()无参有返回值方法，演示return的用法，调用测试
- 定义plus()两个参有返回值方法，调用测试
- 定义testArray()用于生成整型数组填充随机数并返回数组，调用测试
 */
public class MethodDemo {
    public static void main(String[] args) {
        say();

        sayHi("dong");

        sayHello("d",26);

        double a = getNum();
        System.out.println(a);

        int b = plus(20,30);
        System.out.println(b);
        int m=10,n=20;
        int c = plus(m,n);
        System.out.println(c);

        int[] arr = testArray(5,20);
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }

    }

    public static void say(){
        System.out.println("大家好，我叫HSD，今年28岁了");
    }

    public static void sayHi(String name){
        System.out.println("大家好，我叫"+name+"，今年27岁了");
    }

    public static void sayHello(String name,int age){
        if(age>60){
            return;
        }
        System.out.println("大家好，我叫"+name+",今年"+age+"岁了");
    }

    public static double getNum(){
        return 6.66;
    }

    public static int plus(int num1,int num2){
        int num = num1+num2;
        return num;
    }

    public static int[] testArray(int len,int max){
        int[] arr = new int[len];
        for(int i=0;i<arr.length;i++){
            arr[i] = (int)(Math.random()*(max+1));
        }
        return arr;
    }
}
