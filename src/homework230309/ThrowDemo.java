package homework230309;

public class ThrowDemo {
    public static void main(String[] args) {
        Student zs = new Student();
        try {
            zs.setAge(1000);
        }catch(RuntimeException e){
            System.out.println("年龄不合法");
        }
        System.out.println("此人年龄为"+zs.getAge()+"岁");
    }
}
