package homework230309;

import java.io.FileOutputStream;
import java.io.IOException;

public class FinallyDemo2 {
    public static void main(String[] args) {
        System.out.println("程序开始");
        FileOutputStream fos = null;
        try{
            fos = new FileOutputStream("./a/c/b/pw.txt");
            fos.write(1);
        }catch(IOException e){
            System.out.println("异常已经解决");
        }finally {
            try {
                fos.close();
            } catch(Exception e){
                System.out.println("已经关闭流");
            }
        }
        System.out.println("程序结束");
    }
}
