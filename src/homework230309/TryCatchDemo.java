package homework230309;

public class TryCatchDemo {
    public static void main(String[] args) {
        System.out.println("程序开始");
        try{
            String line = "";
            System.out.println(line.charAt(0));
        }catch(NullPointerException e){
            System.out.println("发生空指针异常并处理完了");
        }catch(Exception e){
            System.out.println("异常已解决");
        }
        System.out.println("程序结束");
    }
}
