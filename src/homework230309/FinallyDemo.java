package homework230309;

public class FinallyDemo {
    public static void main(String[] args) {
        System.out.println("程序开始");
        try{
            String line = "";
            System.out.println(line.length());
            //String line = null;
            //System.out.println(line.length());
            return;
        }catch(Exception e){
            System.out.println("程序异常已经处理");
        }finally {
            System.out.println("程序继续执行");
        }
        System.out.println("程序结束");
    }
}
