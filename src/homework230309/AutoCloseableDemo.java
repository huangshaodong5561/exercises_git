package homework230309;

import java.io.FileOutputStream;
import java.io.IOException;

public class AutoCloseableDemo {
    public static void main(String[] args) {
        System.out.println("程序开始");
        try(FileOutputStream fos = new FileOutputStream("pw.txt");){
            fos.write(1);
        }catch(IOException e){
            e.printStackTrace();
        }
        System.out.println("程序结束");
    }
}
