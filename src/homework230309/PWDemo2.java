package homework230309;

import java.io.*;
import java.util.Scanner;

public class PWDemo2 {
    public static void main(String[] args) throws FileNotFoundException {
        FileOutputStream fos = new FileOutputStream("note.txt");
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        BufferedWriter bw = new BufferedWriter(osw);
        PrintWriter pw = new PrintWriter(bw,true);
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入内容，单独输入exit退出");
        while(true){
            String line = scan.nextLine();
            if("exit".equals(line)){
                break;
            }
            pw.println(line);
        }
        System.out.println("写入完毕");
        pw.close();
    }
}
