package homework230309;

public class Student {
    private int age;

    public void setAge(int age) {
        if(age<0 || age>100){
            throw new RuntimeException();
        }
    }

    public int getAge() {
        return age;
    }
}
