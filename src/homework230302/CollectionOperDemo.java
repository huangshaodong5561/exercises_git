package homework230302;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionOperDemo {
    public static void main(String[] args) {
        Collection<String> c = new ArrayList<>();
        c.add("java");
        c.add("ios");
        c.add("android");
        c.add("one");
        c.add("two");
        System.out.println("c:"+c);

        Collection<String> c1 = new ArrayList<>();
        c1.add("one");
        c1.add("java");
        c1.add("c++");
        c1.add("yes");
        System.out.println("c1:"+c1);

        c.addAll(c1);
        System.out.println("c:"+c);

        boolean b = c.containsAll(c1);
        System.out.println(b);

        c.retainAll(c1);
        System.out.println(c);

        c.removeAll(c1);
        System.out.println(c);
    }
}
