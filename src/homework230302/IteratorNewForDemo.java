package homework230302;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class IteratorNewForDemo {
    public static void main(String[] args) {
        Collection<String> c = new ArrayList<>();
        c.add("java");
        c.add("ios");
        c.add("android");
        c.add("one");
        c.add("two");
        System.out.println("c:"+c);

        Iterator<String> it = c.iterator();
        while(it.hasNext()){
            String str = it.next();
            System.out.println(str);
            if("ios".equals(str)){
                it.remove();
            }
        }
        System.out.println(c);

        for(String str : c){
            System.out.println(str);
        }
    }
}
