package homework230302;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class CollectionToArray {
    public static void main(String[] args) {
        Collection<String> c = new ArrayList<>();
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
        System.out.println("c:"+c);

        String[] array = c.toArray(new String[c.size()]);
        System.out.println("array:"+Arrays.toString(array));
    }
}
