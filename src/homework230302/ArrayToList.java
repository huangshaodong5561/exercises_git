package homework230302;

import java.util.Arrays;
import java.util.List;

public class ArrayToList {
    public static void main(String[] args) {
        String[] array = {"one","two","three","four","five"};
        System.out.println(Arrays.toString(array));

        List<String> list = Arrays.asList(array);
        System.out.println("list:"+list);
    }
}
