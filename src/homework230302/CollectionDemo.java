package homework230302;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionDemo {
    public static void main(String[] args) {
        Collection<String> c = new ArrayList<>();
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
        System.out.println(c.size());
        System.out.println("c:"+c);

        boolean b = c.isEmpty();
        System.out.println(b);

        boolean b1 = c.contains("three");
        System.out.println(b1);

        c.remove("one");
        System.out.println(c);

        c.clear();
        System.out.println(c);
    }
}
