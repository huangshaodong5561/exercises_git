package homework230224;
public class Dog extends Animal implements Swim{
    Dog(String name,int age,String color){
        super(name,age,color);
    }

    void eat(){
        System.out.println(color+"色的"+age+"岁的"+name+"在啃骨头");
    }

    void lookHome(){
        System.out.println(color+"色的"+age+"岁的"+name+"在看家");
    }

    public void swim(){
        System.out.println(color+"色的"+age+"岁的"+name+"在游泳");
    }
}
