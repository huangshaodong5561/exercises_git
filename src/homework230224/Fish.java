package homework230224;
public class Fish extends Animal implements Swim {
    Fish(String name,int age,String color){
        super(name,age,color);
    }

    public void swim(){
        System.out.println(color+"色的"+age+"岁的"+name+"在游泳");
    }

    void eat(){
        System.out.println(color+"色的"+age+"岁的"+name+"在吃小虾");
    }
}
