package homework230224;
public class AnimalTest {
    public static void main(String[] args) {
        Animal animal = new Dog("菜",5,"棕");
        Dog k = (Dog)animal;
        Swim d = (Swim)animal;
        //Fish f = (Fish)animal;
        System.out.println(animal instanceof Dog);
        System.out.println(animal instanceof Swim);
        System.out.println(animal instanceof Fish);

        Master master = new Master();
        Animal dog1 = new Dog("嘿嘿",2,"黑");
        Fish fish1 = new Fish("金锦",3,"金");
        Chick chick1 = new Chick("坤",1,"黄");
        master.feed(dog1);
        master.feed(chick1);
        master.feed(fish1);

        Animal[] animals = new Animal[5];
        animals[0] = new Dog("小黑",2,"黑");
        animals[1] = new Dog("小白",3,"白");
        animals[2] = new Fish("小金",4,"金");
        animals[3] = new Fish("小红",1,"红");
        animals[4] = new Chick("小黄",2,"黄");
        for(int i=0;i<animals.length;i++){
            System.out.println(animals[i].name);
            animals[i].drink();
            animals[i].eat();
            if(animals[i] instanceof Dog){
                Dog dog = (Dog)animals[i];
                dog.lookHome();
            }
            if(animals[i] instanceof Chick){
                Chick chick = (Chick)animals[i];
                chick.layEggs();
            }
            if(animals[i] instanceof Swim){
                Swim s = (Swim)animals[i];
                s.swim();
            }
        }
    }
}
