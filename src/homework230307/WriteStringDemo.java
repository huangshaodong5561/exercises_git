package homework230307;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class WriteStringDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("data.txt",true);
        String line = "刮风这天我试过握着你手";
        byte[] data = line.getBytes(StandardCharsets.UTF_8);
        fos.write(data);
        fos.write("但偏偏雨渐渐大到我看你不见".getBytes(StandardCharsets.UTF_8));
        System.out.println("写入完毕");
        fos.close();
    }
}
