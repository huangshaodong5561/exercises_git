package homework230307;

import java.io.FileOutputStream;
import java.io.IOException;

public class FOSDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("test.txt");
        fos.write(1);
        fos.write(3);
        System.out.println("写入完毕");
        fos.close();
    }
}
