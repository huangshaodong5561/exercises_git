package homework230307;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyDemo2 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("JAVA.jpg");
        FileOutputStream fos = new FileOutputStream("JAVA3.jpg");
        long start = System.currentTimeMillis();
        int len;
        byte[] data = new byte[10*1024];
        while((len=fis.read(data))!=-1){
            fos.write(data,0,len);
        }
        long end = System.currentTimeMillis();
        System.out.println("复制完毕！耗时"+(end-start)+"ms");
        fis.close();
        fos.close();
    }
}
