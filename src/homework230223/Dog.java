package homework230223;

public class Dog extends Animal implements Swim{
    Dog(String name,int age,String color){
        super(name,age,color);
    }

    void eat(){
        System.out.println(age+"岁的"+color+"色的"+name+"正在啃骨头");
    }

    void lookHome(){
        System.out.println(age+"岁的"+color+"色的"+name+"正在看家");
    }

    public void swim(){
        System.out.println(age+"岁的"+color+"色的"+name+"正在游泳");
    }
}
