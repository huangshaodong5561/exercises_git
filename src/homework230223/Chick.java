package homework230223;

public class Chick extends Animal {
    Chick(String name,int age,String color){
        super(name,age,color);
    }

    void eat(){
        System.out.println(age+"岁的"+color+"色的"+name+"正在吃小米");
    }

    void layEggs(){
        System.out.println(age+"岁的"+color+"色的"+name+"正在下蛋");
    }
}
