package homework230223;

public class AnimalTest {
    public static void main(String[] args) {
        Dog[] dogs = new Dog[3];
        dogs[0] = new Dog("小黑",2,"黑");
        dogs[1] = new Dog("小白",3,"白");
        dogs[2] = new Dog("小灰",4,"灰");
        System.out.println(dogs[0].name);
        dogs[1].age = 5;
        dogs[2].swim();
        for(int i=0;i<dogs.length;i++){
            System.out.println(dogs[i].name);
            dogs[i].eat();
            dogs[i].lookHome();
        }

        Chick[] chicks = new Chick[2];
        chicks[0] = new Chick("小黄",1,"黄");
        chicks[1] = new Chick("小花",2,"棕");
        for(int i=0;i<chicks.length;i++){
            System.out.println(chicks[i].name);
            chicks[i].layEggs();
            chicks[i].eat();
        }

        Fish[] fish = new Fish[4];
        fish[0] = new Fish("小金",2,"金");
        fish[1] = new Fish("小红",3,"红");
        fish[2] = new Fish("小白",1,"白");
        fish[3] = new Fish("小黑",4,"黑");
        for(int i=0;i<fish.length;i++){
            System.out.println(fish[i].name);
            fish[i].swim();
            fish[i].eat();
        }
    }
}
