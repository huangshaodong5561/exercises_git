package homework230314;

import java.util.Scanner;

/**
 * 实现建议的倒计时程序
 * 程序启动后在控制台上输入一个数字，然后从该数字开始每秒递减，到0时输出"时间到"，然后程序结束
 */
public class SleepDemo {
    public static void main(String[] args) {
        System.out.println("请输入数字");
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println("程序开始");
        for(;num>0;num--){
            System.out.println(num);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("时间到");
        System.out.println("程序结束");
    }
}
