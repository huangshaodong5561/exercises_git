package homework230222;

public class PersonTest {
    public static void main(String[] args) {
        Student zs = new Student("张三",18,"北京","jsd2302","001");
        zs.sayHi();
        zs.eat();
        zs.sleep();
        zs.study();

        Teacher ls = new Teacher("李四",28,"东莞",10000);
        ls.sayHi();
        ls.eat();
        ls.sleep();
        ls.teach();

        Doctor ww = new Doctor("王五",30,"广州","主任");
        ww.sayHi();
        ww.sleep();
        ww.eat();
        ww.cut();
    }
}
