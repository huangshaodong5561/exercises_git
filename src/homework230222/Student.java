package homework230222;

public class Student extends Person{
    String className;
    String stuId;

    Student(String name,int age,String address,String className,String stuId){
        super(name,age,address);
        this.className = className;
        this.stuId = stuId;
    }

    void study(){
        System.out.println(name+"在学习");
    }

    void sayHi(){
        System.out.println("大家好，我叫"+name+"，今年"+age+"岁了，家住"+address+"班级名称为："+className+"学号为："+stuId);
    }
}
