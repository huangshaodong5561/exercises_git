package homework230222;

public class AnimalTest {
    public static void main(String[] args) {
        Chick chick = new Chick("坤坤",2,"黄");
        chick.drink();
        chick.eat();
        chick.layEggs();

        Dog dog = new Dog("小短腿",3,"白");
        dog.drink();
        dog.eat();
        dog.lookHome();

        Fish fish = new Fish("小金",1,"红");
        fish.drink();
        fish.eat();
    }
}
