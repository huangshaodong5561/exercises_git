package homework230222;

public class Animal {
    String name;
    int age;
    String color;

    Animal(String name,int age,String color){
        this.name = name;
        this.age = age;
        this.color = color;
    }

    void eat(){
        System.out.println(age+"岁的"+color+"色的"+name+"正在吃东西");
    }

    void drink(){
        System.out.println(age+"岁的"+color+"色的"+name+"正在喝水");
    }
}
